<!--
This README describes the package. If you publish this package to pub.dev,
this README's contents appear on the landing page for your package.

For information about how to write a good package README, see the guide for
[writing package pages](https://dart.dev/guides/libraries/writing-package-pages).

For general information about developing packages, see the Dart guide for
[creating packages](https://dart.dev/guides/libraries/create-library-packages)
and the Flutter guide for
[developing packages and plugins](https://flutter.dev/developing-packages).
-->

Implements a bi-directional secure communication layer based on the Double Ratchet
Algorithm proposed by Trevor Perrin and Moxie Marlinspike in 2013.

## Features

- Supports X3DH (Triple-Diffie-Hellman) Key exhange for setting up a secure session over insecure networks.
- Implements the Double Ratchet Algorithm for exchanging encrypted messages over insecure networks.
- Session Builder for setting up secure sessions with ease
- Flexible configuration of session building through providers to match specific needs (such as integrating your own identity and signalling servers)

## Getting started

The easiest approach for setting up a (bi-directional) secure session between two parties A(lice) and B(ob) is to use the Session Builder. This requires implementation of various interfaces:

- a Key Storage is required for storing key material securely on the local device. If in-memory storage for key material is sufficient, you can stick with the X3DHInMemoryKeyStorage provided with this package.

- a Key Provider implements generation of fresh key material for use with the X3DH and Double Ratchet Algorithm. You can use the Key Provider implementation that comes with this package unless you want to use different means to create the required key material (such as using a cryptoprocessor).

- a Key Package Publisher implements retrieval or generation of key material for use with the X3DH and Double Ratchet Algorithm. Usually, you must implement your own key package publisher to integrate a specific identity service used by your software system.

- a Messaging Service performs the actual reliable data transport from a client to a remote endpoint. You need to implement your own message service in order to integrate your own messaging server.

The Secure Session Builder takes implementations of the aforementioned interfaces as input and can create secure sessions:

```dart
// Create identity of Alice. This basically creates a X3DH keypair for use with the integral key exchange protocol.
final aliceId = X3DHIdentity.create();

// Create required services for key storage, generation and key package publishing, as well as data transport.
final keyStorageAlice = X3DHInMemoryKeyStorage();
final keyProviderAlice = X3DHDefaultKeyProvider();
final keyPackagePublisherAlice =
    X3DHInMemoryKeyPackagePublisher(identity: aliceId);
final sessionMessageServiceAlice =
    InMemoryMessagingService(localIdentity: aliceId.id);

// Create secure session builder for Alice. This builder can then be used to create an arbitrary number of sessions.
final builderAlice = SecureSessionBuilder(
    applicationIdentifier: "some.test.application", // a unique identifier of your application (e.g. the bundle identifier)
    localIdentity: aliceId,
    keyStorage: keyStorageAlice,
    keyProvider: keyProviderAlice,
    keyPackagePublisher: keyPackagePublisherAlice,
    sessionMessageService: sessionMessageServiceAlice,
);

// Request the Alice' session builder to prepare for operation.
await builderAlice.setup();

// At this point, we can use Alice' session builder to setup numerous secure sessions to other users.
// In this particular case, we want to establish a secure session wuth Bob, so we need to know Bob's identity.
final secureSessionAlice = await builderAlice.build(bobId);

// On Bob's side, we assume that his session builder has been created and setup. Additionally, we assume that Bob has been
// informed on a new session from Alice (signalling is out of scope of this package). So, Bob now needs to process
// the first message, with just is a HELLO message:
final helloMessage = await sessionMessageServiceBob.receiveNext();

// The received HELLO message is now required by Bob to accept the incoming session request, which eventually will build
// a secure session that is logically associated with Alice' secure session:
final secureSessionBob = await builderBob.acceptSession(helloMessage);

// So, with Alice' and Bob's respective secure sessions linked with each other, we can exchange encrypted messages.
// In this example, we have ALice sending a secured message to Bob:
final payloadAlice = Uint8List.fromList("Hello World from Alice!".codeUnits);
await secureSessionAlice.send(payloadAlice);

// This sends the (encrypted) payload from Alice to Bob. So, Bob should now read the encrypted payload and decrypt it:
final receivedMessage = await secureSessionBob.receive();

// This yields the original (unencrypted) payload that Alice sent to Bob. Bob can now wait for another message sent by Alice,
// or he can send back his own message to Alice.
```

## Usage

```dart
final aliceId = X3DHIdentity.create();
final keyStorageAlice = X3DHInMemoryKeyStorage();
final keyProviderAlice = X3DHDefaultKeyProvider();
final keyPackagePublisherAlice = X3DHInMemoryKeyPackagePublisher(identity: aliceId);
final sessionMessageServiceAlice = InMemoryMessagingService(localIdentity: aliceId.id);

final bobId = X3DHIdentity.create();
final keyStorageBob = X3DHInMemoryKeyStorage();
final keyProviderBon = X3DHDefaultKeyProvider();
final keyPackagePublisherBob = X3DHInMemoryKeyPackagePublisher(identity: bobId);
final sessionMessageServiceBob = InMemoryMessagingService(localIdentity: bobId.id);

final builderAlice = SecureSessionBuilder(
    applicationIdentifier: "some.test.application", // a unique identifier of your application (e.g. the bundle identifier)
    localIdentity: aliceId,
    keyStorage: keyStorageAlice,
    keyProvider: keyProviderAlice,
    keyPackagePublisher: keyPackagePublisherAlice,
    sessionMessageService: sessionMessageServiceAlice,
);

await builderAlice.setup();

final secureSessionAlice = await builderAlice.build(bobId);

final builderBob = SecureSessionBuilder(
    applicationIdentifier: "some.test.application", // a unique identifier of your application (e.g. the bundle identifier)
    localIdentity: bobId,
    keyStorage: keyStorageBob,
    keyProvider: keyProviderBob,
    keyPackagePublisher: keyPackagePublisherBob,
    sessionMessageService: sessionMessageServiceBob,
);

await builderBob.setup();
final helloMessage = await sessionMessageServiceBob.receiveNext();
final secureSessionBob = await builderBob.acceptSession(helloMessage);

final payloadAlice = Uint8List.fromList("Hello World from Alice!".codeUnits);
await secureSessionAlice.send(payloadAlice);
final receivedMessage = await secureSessionBob.receive();
```

## Additional information

TODO: Tell users more about the package: where to find more information, how to
contribute to the package, how to file issues, what response they can expect
from the package authors, and more.
