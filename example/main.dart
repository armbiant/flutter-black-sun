// ignore_for_file: avoid_print

import 'package:flutter/foundation.dart';
import 'package:flutter_double_ratchet/src/security/message_service.dart';
import 'package:flutter_double_ratchet/src/security/secure_session_builder.dart';
import 'package:flutter_double_ratchet/src/security/x3dh_key_package_publisher.dart';
import 'package:flutter_double_ratchet/src/security/x3dh_key_provider.dart';
import 'package:flutter_double_ratchet/src/security/x3dh_key_storage.dart';

void main() async {
  // In this scenario, Alice uses a SecureSessionBuilder to create a
  // SecureSession that is connected to Bob.

  // SETUP ALICE

  // Create identity of Alice
  final aliceId = X3DHIdentity.create();

  // Create required (mock) services
  final keyStorageAlice = X3DHInMemoryKeyStorage();
  final keyProviderAlice = X3DHDefaultKeyProvider();
  final keyPackagePublisherAlice =
      X3DHInMemoryKeyPackagePublisher(identity: aliceId);
  final sessionMessageServiceAlice =
      InMemoryMessagingService(localIdentity: aliceId.id);

  // Create secure session builder. This builder can then be used to create
  // an arbitrary number of sessions.
  final builderAlice = SecureSessionBuilder(
    applicationIdentifier: "some.test.application",
    localIdentity: aliceId,
    keyStorage: keyStorageAlice,
    keyProvider: keyProviderAlice,
    keyPackagePublisher: keyPackagePublisherAlice,
    sessionMessageService: sessionMessageServiceAlice,
  );

  await builderAlice.setup();

  // SETUP BOB

  // Create identity of Bob
  final bobId = X3DHIdentity.create();

  // Create required (mock) services for Bob.
  final keyStorageBob = X3DHInMemoryKeyStorage();
  final keyProviderBob = X3DHDefaultKeyProvider();
  final keyPackagePublisherBob =
      X3DHInMemoryKeyPackagePublisher(identity: bobId);
  final sessionMessageServiceBob =
      InMemoryMessagingService(localIdentity: bobId.id);

  // Create secure session builder for Bob.
  // This builder can then be used to create
  // an arbitrary number of sessions.
  final builderBob = SecureSessionBuilder(
    applicationIdentifier: "some.test.application",
    localIdentity: bobId,
    keyStorage: keyStorageBob,
    keyProvider: keyProviderBob,
    keyPackagePublisher: keyPackagePublisherBob,
    sessionMessageService: sessionMessageServiceBob,
  );

  await builderBob.setup();

  // SETUP FRESH SESSION BETWEEN ALICE AND BOB

  // Alice now uses the session builder to create a secure session that is
  // connected to Bob.
  final secureSessionAlice = await builderAlice.build(bobId);

  // In this test, we can safely assume at this point that there's a
  // HELLO message waiting for being processed by Bob.
  final helloMessage = await sessionMessageServiceBob.receiveNext();

  if (helloMessage != null) {
    // Now, we let Bob build the remote part of the session to complete setup.
    final secureSessionBob = await builderBob.acceptSession(helloMessage);

    // At this point, session linkage should be already successful. We now
    // exchange some data messages between Alice and Bob to see, if session
    // setup was really successful.
    final payloadAlice =
        Uint8List.fromList("Hello World from Alice!".codeUnits);
    await secureSessionAlice.send(payloadAlice);

    // Now let Bob recive the transferred data.
    final receivedMessage = await secureSessionBob.receive();

    print("Original: $payloadAlice");
    print("Received: $receivedMessage");
  }
}
