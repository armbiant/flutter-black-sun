import 'dart:convert';

import 'user_manager.dart';
import 'package:http/http.dart' as http;
import 'package:jwt_decoder/jwt_decoder.dart';

class SquibbleIdentitySession {
  final String accessToken;
  final String subject;
  final int expiration;
  final int issued;

  SquibbleIdentitySession({
    required this.accessToken,
    required this.subject,
    required this.expiration,
    required this.issued,
  });
}

// Implements access to the public Squibble Identity API
class SquibbleIdentityService {
  final Uri baseUrl;

  SquibbleIdentityService({
    required this.baseUrl,
  });

  // Registers a new user with the Squibble Identity API and returns the
  // resulting user descriptor.
  Future<User> register(String name, String email, String? password) async {
    var url = Uri.parse("$baseUrl/auth/register");
    var response = await http.post(
      url,
      headers: {
        "Content-Type": "application/json; charset=utf-8",
      },
      body: jsonEncode(
        <String, String?>{
          "name": name,
          "email": email,
          "password": password,
        },
      ),
    );

    if (response.statusCode == 201) {
      // Registration succeeded -> extract user data
      var responseData = jsonDecode(response.body);
      return User(
        address: UserAddress(id: responseData["id"]),
        email: responseData["email"],
        name: responseData["name"],
      );
    } else {
      throw Exception("Registration failed.");
    }
  }

  // Sets a new password for the given user.
  Future<void> resetPassword(
    SquibbleIdentitySession session,
    String newPassword,
  ) async {
    var url = Uri.parse(
      "$baseUrl/users/${session.subject}/password",
    );
    var response = await http.patch(
      url,
      headers: <String, String>{
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": "Bearer ${session.accessToken}",
      },
      body: jsonEncode(
        <String, String>{"password": newPassword},
      ),
    );

    if (response.statusCode == 200) {
      // Change successful.
      return;
    } else {
      throw Exception("Could not update user's password");
    }
  }

  // Establishes a new Squibble session by email and password credential flow.
  Future<SquibbleIdentitySession> loginByEmailAndPassword(
    String email,
    String password,
  ) async {
    // Construct and send request
    var url = Uri.parse("$baseUrl/auth/login");
    var response = await http.post(
      url,
      headers: {
        "Content-Type": "application/json; charset=utf-8",
      },
      body: jsonEncode(
        <String, String>{
          "email": email,
          "password": password,
        },
      ),
    );

    // Evaluate response
    if (response.statusCode == 201) {
      // Ok -> extract JWT data and put them into SquibbleIdentitySession
      var jwt = jsonDecode(
        response.body,
      )["access_token"];
      Map<String, dynamic> decodedToken = JwtDecoder.decode(jwt);
      return SquibbleIdentitySession(
        accessToken: jwt,
        subject: decodedToken["sub"],
        expiration: decodedToken["exp"],
        issued: decodedToken["iss"],
      );
    } else {
      throw Exception("Failed to login.");
    }
  }
}
