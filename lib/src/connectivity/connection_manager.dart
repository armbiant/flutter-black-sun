import 'user_manager.dart';

/// Defines a single connection between two Squibble users.
class Connection {
  final User localUser;
  final User remoteUser;

  Connection({
    required this.localUser,
    required this.remoteUser,
  });
}

/// Manages connections between Squibble users ("Squibblers").
abstract class ConnectionManager {
  /// Creates a new connection to the given Squibble user.
  Future<void> create(UserAddress toUser);

  /// Drops the current connection to the given Squibble user.
  Future<void> drop(UserAddress toUser);

  /// Returns the current connections of the local user to other users.
  Future<List<Connection>> connections();
}
