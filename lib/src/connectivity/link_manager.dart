import 'user_manager.dart';

/// Defines the address of a Link endpoint.
class EndpointAddress {
  UserAddress userAddress;
  String deviceId;

  EndpointAddress({
    required this.userAddress,
    required String deviceId,
  }) : deviceId = deviceId.toLowerCase();

  /// Returns the stringified address in canonical format.
  String get address {
    return "${userAddress.address}:$deviceId";
  }
}

/// Defines an endpoint of a Link.
class Endpoint {
  EndpointAddress address;
  Endpoint({
    required this.address,
  });
}

/// Defines a Link between two Endpoints.
class Link {
  String id;
  Endpoint localEndpoint;
  Endpoint remoteEndpoint;

  Link({
    required this.id,
    required this.localEndpoint,
    required this.remoteEndpoint,
  });
}

/// Manages the links established between endpoints of a private social network.
abstract class LinkManager {
  /// Creates a new link between the local endpoint and the given remote
  /// endpoint.
  Future<Link> create(EndpointAddress toEndpoint);

  /// Destroys the given link between the local endpoint and the given remote
  /// endpoint.
  Future<void> drop(Link linkToDestroy);

  /// Returns all links maintained by this manager.
  Future<List<Link>> links();
}
