import 'link_manager.dart';

/// Defines the address for a particular Squibble user.
class UserAddress {
  final String address;
  UserAddress({
    required String id,
  }) : address = id.toLowerCase();
}

/// Defines model for an individual Squibble user.
class User {
  final UserAddress address;
  final String? name;
  final String email;

  User({
    required this.address,
    this.name,
    required this.email,
  });
}

/// Manages Squibble users on behalf ofthe local user.
abstract class UserManager {
  /// Stores a new user data record on behalf of the local user.
  Future<void> registerLocal(User newUser);

  /// Fetches the data record for a given user.
  Future<User> userByAddress(UserAddress userAddress);

  /// Attaches and registers an endpoint for the local user.
  Future<void> registerEndpointForLocalUser(Endpoint newEndpoint);

  /// Fetches the currently registered and active endpoints for a given user.
  Future<List<Endpoint>> endpointsOfUser(UserAddress userAddress);

  /// Returns the local user data record (if already set)
  Future<User?> get localUser;
}
