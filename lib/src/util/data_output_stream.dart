import 'dart:convert';
import 'dart:typed_data';

class DataOutputStream {
  final List<int> _buffer = [];

  void reset() {
    _buffer.clear();
  }

  void writeBytes(Uint8List bytes) {
    // Write length of bytes first
    writeInt32(bytes.length);
    // Write bytes
    _buffer.addAll(bytes);
  }

  void writeString(String string) {
    // Write length of string first
    writeInt32(string.length);
    // Write utf-8 string data
    _buffer.addAll(utf8.encode(string));
  }

  void writeInt32(int integer) {
    // Write 32bit integer in network order
    final bytes = Uint8List(4)
      ..buffer.asByteData().setInt32(0, integer, Endian.big);
    _buffer.addAll(bytes);
  }

  void writeInt(int integer) {
    // Write 64bit integer in network order
    final bytes = Uint8List(8)
      ..buffer.asByteData().setInt64(0, integer, Endian.big);
    _buffer.addAll(bytes);
  }

  void writeFloat(double float) {
    final bytes = Uint8List(4)
      ..buffer.asByteData().setFloat32(0, float, Endian.big);
    _buffer.addAll(bytes);
  }

  void writeDouble(double double) {
    final bytes = Uint8List(8)
      ..buffer.asByteData().setFloat64(0, double, Endian.big);
    _buffer.addAll(bytes);
  }

  void writeBool(bool bool) {
    final bytes = Uint8List(1)..buffer.asByteData().setUint8(0, bool ? 1 : 0);
    _buffer.addAll(bytes);
  }

  void writeByte(int byte) {
    final bytes = Uint8List(1)..buffer.asByteData().setUint8(0, byte);
    _buffer.addAll(bytes);
  }

  // Returns the content of this stream
  Uint8List get byteBuffer {
    return Uint8List.fromList(_buffer);
  }

  // Returns the current length of this stream (in bytes)
  int get length {
    return _buffer.length;
  }
}
