import 'dart:convert';
import 'dart:typed_data';

class DataInputStream {
  final Uint8List buffer;
  int _offset = 0;

  DataInputStream({required this.buffer});

  int readInt32() {
    assert(buffer.length - _offset >= 4);
    final result = buffer.buffer.asByteData().getInt32(_offset, Endian.big);
    _offset += 4;
    return result;
  }

  int readInt() {
    assert(buffer.length - _offset >= 8);
    final result = buffer.buffer.asByteData().getInt64(_offset, Endian.big);
    _offset += 8;
    return result;
  }

  String readString() {
    // Read length first
    final length = readInt32();
    // Read string data (as UTF-8)
    assert(buffer.length - _offset >= length);
    final stringData = buffer.buffer.asUint8List(_offset, length);
    _offset += length;
    return utf8.decode(stringData);
  }

  Uint8List readBytes() {
    // Read length first
    final length = readInt32();
    // Read byte data
    assert(buffer.length - _offset >= length);
    final bytes = buffer.buffer.asUint8List(_offset, length);
    _offset += length;
    return bytes;
  }

  bool readBool() {
    assert(buffer.length - _offset >= 1);
    final result = buffer.buffer.asByteData().getUint8(_offset);
    _offset++;
    return result == 1;
  }

  double readFloat() {
    assert(buffer.length - _offset >= 4);
    final result = buffer.buffer.asByteData().getFloat32(_offset, Endian.big);
    _offset += 4;
    return result;
  }

  double readDouble() {
    assert(buffer.length - _offset >= 8);
    final result = buffer.buffer.asByteData().getFloat64(_offset, Endian.big);
    _offset += 8;
    return result;
  }

  int readByte() {
    assert(buffer.length - _offset >= 1);
    final result = buffer.buffer.asByteData().getUint8(_offset);
    _offset++;
    return result;
  }
}
