import 'dart:typed_data';

import 'package:cryptography/cryptography.dart';
import '../util/data_input_stream.dart';
import '../util/data_output_stream.dart';

/// Implements the double ratchet message header preceding the ciphertext of
/// an encrypted message.
class DoubleRatchetMessageHeader {
  final SimplePublicKey publicDhKey;
  final int previousChainLength;
  final int messageNumber;

  DoubleRatchetMessageHeader({
    required this.publicDhKey,
    required this.previousChainLength,
    required this.messageNumber,
  });

  void toOutputStream(DataOutputStream outputStream) {
    outputStream.writeBytes(
      Uint8List.fromList(
        publicDhKey.bytes,
      ),
    );
    outputStream.writeInt(messageNumber);
    outputStream.writeInt32(previousChainLength);
  }

  static DoubleRatchetMessageHeader fromInputStream(
    DataInputStream inputStream,
  ) {
    final publicDhKeyBytes = inputStream.readBytes();
    final publicDhKey = SimplePublicKey(
      publicDhKeyBytes,
      type: KeyPairType.x25519,
    );
    final messageNumber = inputStream.readInt();
    final prevChainLength = inputStream.readInt32();
    return DoubleRatchetMessageHeader(
      publicDhKey: publicDhKey,
      previousChainLength: prevChainLength,
      messageNumber: messageNumber,
    );
  }
}
