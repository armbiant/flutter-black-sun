import 'dart:collection';
import 'dart:typed_data';

import 'package:cryptography/cryptography.dart';
import 'x3dh_key_package_publisher.dart';

class X3DHKey {
  // Id for the key
  final String id;
  // The keypair
  final SimpleKeyPair keyPair;
  // The UTC timestamp this key was created
  final int createdAt;

  X3DHKey({
    required this.id,
    required this.keyPair,
    required this.createdAt,
  });

  // Extracts the public key part of this key.
  Future<X3DHPublicKey> publicKey() async {
    final pubKey = await keyPair.extractPublicKey();
    return X3DHPublicKey(id: id, key: pubKey);
  }
}

class X3DHSignedKey extends X3DHKey {
  final Uint8List signature;

  X3DHSignedKey({
    required String id,
    required SimpleKeyPair keyPair,
    required int createdAt,
    required this.signature,
  }) : super(id: id, keyPair: keyPair, createdAt: createdAt);
}

/// Declares the interface for a key storage used during X3DH protocol.
abstract class X3DHKeyStorage {
  /// Stores given identity key.
  Future storeIdentityKey(X3DHKey key);

  // Stores given signing key.
  Future storeSigningKey(X3DHKey key);

  /// Stores given signed prekey.
  Future storeSignedPrekey(X3DHSignedKey key);

  /// Stores given one-time prekeys
  Future storeOneTimePrekeys(List<X3DHKey> keys);

  /// Fetches the identity key.
  Future<X3DHKey?> fetchIdentityKey();

  /// Fetches the signing key.
  Future<X3DHKey?> fetchSigningKey();

  /// Fetches a signed prekey given by its id.
  Future<X3DHSignedKey?> fetchSignedPrekey(String id);

  /// Fetches the active signed prekey (if available).
  Future<X3DHSignedKey?> fetchActiveSignedPrekey();

  Future<List<X3DHSignedKey>> removeExpiredSignedKeys();

  /// Fetches a one-time prekey given by its id.
  Future<X3DHKey?> fetchOneTimePrekey(String id);

  /// Fetches all available one-time prekeys.
  Future<List<X3DHKey>> fetchOneTimePrekeys();

  /// Deletes a given signed prekey
  Future deleteSignedPrekey(String id);

  // Deletes a given one-time prekey
  Future deleteOneTimePrekey(String id);
}

/// Implements key storage interface using an in-memory database.
class X3DHInMemoryKeyStorage implements X3DHKeyStorage {
  // Map signed prekeys id to key
  final _signedPrekeyMap = HashMap<String, X3DHSignedKey>();
  // Map one-time prekeys id to key
  final _oneTimePrekeyMap = HashMap<String, X3DHKey>();
  // The current identity key
  X3DHKey? _identityKey;
  // The current signing key
  X3DHKey? _signingKey;

  @override
  Future deleteOneTimePrekey(String id) async {
    _oneTimePrekeyMap.remove(id);
  }

  @override
  Future deleteSignedPrekey(String id) async {
    _signedPrekeyMap.remove(id);
  }

  @override
  Future<X3DHKey?> fetchIdentityKey() async {
    return _identityKey;
  }

  @override
  Future<X3DHKey?> fetchSigningKey() async {
    return _signingKey;
  }

  @override
  Future<X3DHKey?> fetchOneTimePrekey(String id) async {
    return _oneTimePrekeyMap[id];
  }

  @override
  Future<List<X3DHKey>> fetchOneTimePrekeys() async {
    return _oneTimePrekeyMap.values.toList();
  }

  @override
  Future<X3DHSignedKey?> fetchSignedPrekey(String id) async {
    return _signedPrekeyMap[id];
  }

  @override
  Future<X3DHSignedKey?> fetchActiveSignedPrekey() async {
    // A prekey is considered active, if it is not older than X seconds.
    final now = DateTime.now().millisecondsSinceEpoch;

    for (X3DHSignedKey key in _signedPrekeyMap.values) {
      if (now - key.createdAt < 3600 * 1000) {
        // Key is active!
        return key;
      }
    }

    return null;
  }

  @override
  Future<List<X3DHSignedKey>> removeExpiredSignedKeys() async {
    // A signed key is considered to be expired, if its age is over
    // a certain threshold
    final now = DateTime.now().millisecondsSinceEpoch;
    final List<X3DHSignedKey> result = [];

    for (X3DHSignedKey key in _signedPrekeyMap.values) {
      if (now - key.createdAt > 4 * 3600 * 1000) {
        // Key is expired -> remove
        result.add(key);
      }
    }

    for (X3DHSignedKey key in result) {
      _signedPrekeyMap.remove(key.id);
    }

    return result;
  }

  @override
  Future storeIdentityKey(X3DHKey key) async {
    _identityKey = key;
  }

  @override
  Future storeSigningKey(X3DHKey key) async {
    _signingKey = key;
  }

  @override
  Future storeOneTimePrekeys(List<X3DHKey> keys) async {
    for (X3DHKey key in keys) {
      _oneTimePrekeyMap[key.id] = key;
    }
  }

  @override
  Future storeSignedPrekey(X3DHSignedKey key) async {
    _signedPrekeyMap[key.id] = key;
  }
}
