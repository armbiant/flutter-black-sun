import 'package:cryptography/cryptography.dart';
import 'x3dh_key_package_publisher.dart';
import 'x3dh_key_provider.dart';
import 'x3dh_key_storage.dart';

/// Defines the private output from a X3DH protocol invocation. The data herein
/// must be kept private by the local identity.
class X3DHPrivateParameters {
  final SecretKey sharedKey;
  final List<int> associatedData;

  X3DHPrivateParameters({
    required this.sharedKey,
    required this.associatedData,
  });
}

/// Defines the public output from a X3DH protocol invocation. The data herein
/// can be shared over unsecure channels without compromise of security.
class X3DHPublicParameters {
  final X3DHPublicKey initiatingIdentityKey;
  final X3DHPublicKey remoteIdentityKey;
  final X3DHPublicKey ephemeralKey;
  final X3DHPublicKey remotePrekey;
  final String? usedOneTimePrekeyId;

  X3DHPublicParameters({
    required this.initiatingIdentityKey,
    required this.remoteIdentityKey,
    required this.ephemeralKey,
    required this.remotePrekey,
    this.usedOneTimePrekeyId,
  });
}

/// Defines the output from a single X3DH protocol run, when initiated by the
/// local party. Consists of both a public and a private data part.
/// Protocol parameters are fixed to Curve25519 for ECDH and ECDSA, using a
/// SHA-512 hash for KDF.
class X3DHOutput {
  final X3DHPrivateParameters privateParameters;
  final X3DHPublicParameters publicParameters;

  X3DHOutput({
    required this.privateParameters,
    required this.publicParameters,
  });
}

/// Implements the core of the X3DH Key Agreement Protocol as specified in
/// https://signal.org/docs/specifications/x3dh/
class X3DH {
  final String applicationId;
  final X3DHIdentity localIdentity;
  final X3DHKeyStorage storage;
  final X3DHKeyProvider keyProvider;
  final X3DHKeyPackagePublisher keyPackagePublisher;
  final int oneTimePrekeyBatchSize;
  final int minNumberOfOneTimePrekeysRequired;

  X3DH({
    required this.applicationId,
    required this.localIdentity,
    required this.storage,
    required this.keyProvider,
    required this.keyPackagePublisher,
    this.oneTimePrekeyBatchSize = 100,
    this.minNumberOfOneTimePrekeysRequired = 10,
  });

  /// Convenience function that sets up the local identity for X3DH protocol.
  /// Please call this function (and always wait for its completion!) before
  /// initiating the X3DH protocol.
  Future setup() async {
    // (1) Check if an identity key is available. If not, install one.
    X3DHKey? identityKey = await storage.fetchIdentityKey();

    if (identityKey == null) {
      // No identity key found -> create and store one
      identityKey = await keyProvider.generateKey();
      await storage.storeIdentityKey(identityKey);
    }

    // (2) Check, if an signing key is available. If not, install one.
    X3DHKey? signingKey = await storage.fetchSigningKey();

    if (signingKey == null) {
      // No signing key found -> create and store one
      signingKey = await keyProvider.generateSigningKey();
      await storage.storeSigningKey(signingKey);
    }

    // (3) Check if an active signed prekey is available.
    // If not (or if the key is too old), install a new one.
    X3DHSignedKey? prekey = await storage.fetchActiveSignedPrekey();

    if (prekey == null) {
      // No active pre key found, or existing prekey is too old -> make new key
      prekey = await keyProvider.generateSignedKey(signingKey);
      await storage.storeSignedPrekey(prekey);
    }

    // (4) Check, if there's a sufficient number of one-time prekeys available.
    // If not, we need to create a new batch of those.
    List<X3DHKey> oneTimePreKeys = await storage.fetchOneTimePrekeys();

    if (oneTimePreKeys.length < minNumberOfOneTimePrekeysRequired) {
      // Not enough one-time prekeys left -> create a new batch
      final List<X3DHKey> newBatch = [];
      for (int i = 0; i < oneTimePrekeyBatchSize; i++) {
        final newKey = await keyProvider.generateKey();
        newBatch.add(newKey);
      }
      await storage.storeOneTimePrekeys(newBatch);
      oneTimePreKeys.addAll(newBatch);
    }

    // (5) Delete signed prekeys that are too old.
    await storage.removeExpiredSignedKeys();

    // (6) Publish/update the keypackage for our local identity.
    final List<X3DHPublicKey> otk =
        await Future.wait(oneTimePreKeys.map((k) => k.publicKey()));

    final X3DHKeyPackage keyPackage = X3DHKeyPackage(
      identity: localIdentity,
      identityKey: await identityKey.publicKey(),
      prekey: await prekey.publicKey(),
      prekeySignature: prekey.signature,
      signingKey: await signingKey.publicKey(),
      oneTimePrekeys: otk,
    );

    await keyPackagePublisher.publish(keyPackage);
  }

  /// Executes the X3DH protocol as initiator. This results a X3DH output, which
  /// then can be applied to any post-X3DH protocol for communication.
  Future<X3DHOutput> initiateProtocol(
    X3DHIdentity remoteIdentity,
  ) async {
    // (1) Initiator needs to fetch the key package of the remote identity
    final remoteKeyPackage = await keyPackagePublisher.fetch(remoteIdentity);
    // The remote key package must exist in order to continue with the protocol.
    assert(remoteKeyPackage != null);

    // (2) Initiator then needs to verify the signature of the prekey in the
    // downloaded key package.
    final remoteIdentityKey = remoteKeyPackage!.identityKey.key.bytes;
    final remotePrekey = remoteKeyPackage.prekey.key.bytes;
    final sigPublicKey = SimplePublicKey(
      remoteKeyPackage.signingKey.key.bytes,
      type: KeyPairType.ed25519,
    );
    final sig = Ed25519();
    final signatureValid = await sig.verify(
      remotePrekey,
      signature: Signature(
        remoteKeyPackage.prekeySignature,
        publicKey: sigPublicKey,
      ),
    );
    // Signature must be valid in order to progress with protocol
    assert(signatureValid);

    // (3) Initiator now generates ephemeral key pair
    final ephemeralKey = await keyProvider.generateKey();

    // (4) Initiator now calculates a shared key by section 3.3 of X3DH
    final identityKey = await storage.fetchIdentityKey();
    assert(identityKey != null);

    final dh1 = await _dh(
      identityKey!.keyPair,
      remoteKeyPackage.prekey.key,
    );
    final dh2 = await _dh(
      ephemeralKey.keyPair,
      remoteKeyPackage.identityKey.key,
    );
    final dh3 = await _dh(
      ephemeralKey.keyPair,
      remoteKeyPackage.prekey.key,
    );

    SecretKey sharedKey;
    String? otkId;

    if (remoteKeyPackage.oneTimePrekeys.isEmpty) {
      sharedKey = await _kdf(
        (await dh1.extractBytes()) +
            (await dh2.extractBytes()) +
            (await dh3.extractBytes()),
        applicationId.codeUnits,
      );
    } else {
      final otk = remoteKeyPackage.oneTimePrekeys.first.key;
      otkId = remoteKeyPackage.oneTimePrekeys.first.id;
      final dh4 = await _dh(
        ephemeralKey.keyPair,
        otk,
      );
      sharedKey = await _kdf(
        (await dh1.extractBytes()) +
            (await dh2.extractBytes()) +
            (await dh3.extractBytes()) +
            (await dh4.extractBytes()),
        applicationId.codeUnits,
      );
    }

    // (5) Initiator then calculates Associated Data
    final ad = (await identityKey.publicKey()).key.bytes + remoteIdentityKey;

    // We are done with X3DH now.
    return X3DHOutput(
      privateParameters: X3DHPrivateParameters(
        sharedKey: sharedKey,
        associatedData: ad,
      ),
      publicParameters: X3DHPublicParameters(
        initiatingIdentityKey: await identityKey.publicKey(),
        remoteIdentityKey: remoteKeyPackage.identityKey,
        ephemeralKey: await ephemeralKey.publicKey(),
        remotePrekey: remoteKeyPackage.prekey,
        usedOneTimePrekeyId: otkId,
      ),
    );
  }

  // Completes the X3DH protocol on the remote side. Returns the shared key
  // and associated data established from the X3DH protocol.
  Future<X3DHPrivateParameters> completeProtocol(
    X3DHIdentity initiatingIdentity,
    X3DHPublicParameters wireData,
  ) async {
    final identityKey = await storage.fetchIdentityKey();
    assert(identityKey != null);
    final prekey = await storage.fetchSignedPrekey(wireData.remotePrekey.id);
    assert(prekey != null);
    final otk = wireData.usedOneTimePrekeyId != null
        ? await storage.fetchOneTimePrekey(wireData.usedOneTimePrekeyId!)
        : null;

    final dh1 = await _dh(prekey!.keyPair, wireData.initiatingIdentityKey.key);
    final dh2 = await _dh(
      identityKey!.keyPair,
      wireData.ephemeralKey.key,
    );
    final dh3 = await _dh(
      prekey.keyPair,
      wireData.ephemeralKey.key,
    );

    SecretKey sharedKey;

    if (otk == null) {
      sharedKey = await _kdf(
        (await dh1.extractBytes()) +
            (await dh2.extractBytes()) +
            (await dh3.extractBytes()),
        applicationId.codeUnits,
      );
    } else {
      final dh4 = await _dh(
        otk.keyPair,
        wireData.ephemeralKey.key,
      );
      sharedKey = await _kdf(
        (await dh1.extractBytes()) +
            (await dh2.extractBytes()) +
            (await dh3.extractBytes()) +
            (await dh4.extractBytes()),
        applicationId.codeUnits,
      );
    }

    final ad = wireData.initiatingIdentityKey.key.bytes +
        (await identityKey.publicKey()).key.bytes;

    return X3DHPrivateParameters(
      sharedKey: sharedKey,
      associatedData: ad,
    );
  }

  Future<SecretKey> _dh(
    SimpleKeyPair dhKeyPair,
    SimplePublicKey publicDhKey,
  ) async {
    final dh = X25519();
    return dh.sharedSecretKey(
      keyPair: dhKeyPair,
      remotePublicKey: publicDhKey,
    );
  }

  Future<SecretKey> _kdf(List<int> key, List<int> info) async {
    final input = List<int>.filled(32, 0xff) + key;
    final kdf = Hkdf(hmac: Hmac.sha512(), outputLength: 32);
    final nonce = List<int>.filled(kdf.hmac.macLength, 0x00);
    final secretKey = SecretKey(input);
    return kdf.deriveKey(secretKey: secretKey, nonce: nonce, info: info);
  }
}
