import 'dart:typed_data';

import 'package:cryptography/cryptography.dart';
import 'x3dh_key_storage.dart';
import 'package:uuid/uuid.dart';

/// Interface for a key provider that can provide client with fresh
/// X3DH keys.
abstract class X3DHKeyProvider {
  /// Asks this provider for a new x25519 keypair.
  Future<X3DHKey> generateKey();

  /// Asks this provider for a new ed25519 signing keypair
  Future<X3DHKey> generateSigningKey();

  /// Asks this provider for a new x25519 keypair of the given type and signs
  /// its public key using the specified signing key pair.
  Future<X3DHSignedKey> generateSignedKey(
    X3DHKey signingKey,
  );
}

class X3DHDefaultKeyProvider implements X3DHKeyProvider {
  final _x25519 = X25519();
  final _ed25519 = Ed25519();
  final _uuid = const Uuid();

  @override
  Future<X3DHKey> generateKey() async {
    return X3DHKey(
      id: _uuid.v4(),
      keyPair: await _x25519.newKeyPair(),
      createdAt: DateTime.now().millisecondsSinceEpoch,
    );
  }

  @override
  Future<X3DHKey> generateSigningKey() async {
    return X3DHKey(
      id: _uuid.v4(),
      keyPair: await _ed25519.newKeyPair(),
      createdAt: DateTime.now().millisecondsSinceEpoch,
    );
  }

  @override
  Future<X3DHSignedKey> generateSignedKey(X3DHKey signingKey) async {
    final key = await generateKey();
    final signature = await _ed25519.sign(
      (await key.keyPair.extractPublicKey()).bytes,
      keyPair: signingKey.keyPair,
    );
    return X3DHSignedKey(
      id: key.id,
      keyPair: key.keyPair,
      createdAt: key.createdAt,
      signature: Uint8List.fromList(signature.bytes),
    );
  }
}
