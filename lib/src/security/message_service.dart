import 'dart:collection';
import 'dart:typed_data';

enum SessionMessageType { hello }

class Message {
  final String senderIdentity;
  final String receiverIdentity;
  final Uint8List payload;

  Message({
    required this.senderIdentity,
    required this.receiverIdentity,
    required this.payload,
  });
}

/// Interface for implementations of a message service that sends messages
/// to and fetches messages from remote session endpoint.
abstract class MessagingService {
  MessagingService(String localIdentity);

  // Sends message.
  Future send(
    Uint8List payload, {
    required String toRecipientId,
  });

  // Receives next message (if available) and removes it from the mailbox.
  Future<Message?> receiveNext();

  // Receives all pending messages and removes them from the mailbox.
  Future<List<Message>> receiveAll();

  // Checks how many pending messages are available for the local identity.
  Future<int> messagesAvailable();
}

/// Mock implementation for the Message Service
class InMemoryMessagingService implements MessagingService {
  final String localIdentity;

  // Mocks the central messaging server
  static final _messageBox = HashMap<String, List<Message>>();

  InMemoryMessagingService({
    required this.localIdentity,
  });

  @override
  Future<int> messagesAvailable() async {
    return _messageBox[localIdentity]?.length ?? 0;
  }

  @override
  Future<Message?> receiveNext() async {
    return _messageBox[localIdentity]?.isNotEmpty ?? false
        ? _messageBox[localIdentity]?.removeAt(0)
        : null;
  }

  @override
  Future<List<Message>> receiveAll() async {
    final result = _messageBox[localIdentity] ?? [];
    _messageBox.remove(localIdentity);
    return result;
  }

  @override
  Future send(Uint8List payload, {required String toRecipientId}) async {
    final Message message = Message(
      senderIdentity: localIdentity,
      receiverIdentity: toRecipientId,
      payload: payload,
    );

    final list = _messageBox.putIfAbsent(message.receiverIdentity, () => []);
    list.add(message);
  }
}
