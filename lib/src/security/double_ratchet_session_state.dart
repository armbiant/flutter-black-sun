import 'dart:collection';
import 'dart:typed_data';

import 'package:cryptography/cryptography.dart';

/// Implements a single Double Ratchet session between a sender-receiver pair.
/// Follows the session structure of the Double Ratchet specification as
/// described in section 3.2.
class DoubleRatchetSessionState {
  /// The number of messages that are allowed to be either be lost or delayed.
  int maxSkippableMessages = 10;

  /// Current DH key pair for the local part of this session
  SimpleKeyPair? localDH;

  // Current public DH key for the remote part of this session
  SimplePublicKey? remoteDH;

  // Current 32-byte Root Key for this session
  SecretKey? rootKey;

  // Receiving and sending chain keys (32 bytes each)
  SecretKey? sendingChainKey;
  SecretKey? receivingChainKey;

  // Message numbers for sending and receiving
  int sendingMessageNumber = 0;
  int receivingMessageNumber = 0;

  // Number of messaged in previous sending chain.
  int previousSendingMessageNumber = 0;

  // Dictionary of skipped-over message keys. Maps ratchet public key and
  // message number to associated message key.
  final HashMap skippedMessageKeys = HashMap<Uint8List, SecretKey>();
}
