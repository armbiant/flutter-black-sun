enum DoubleRatchetExceptionCause {
  /// The total number of messages that can be skipped on receiver chain
  /// has been exceeded for the current session.
  maxSkippableMessagesExceeded,
}

class DoubleRatchetException implements Exception {
  final DoubleRatchetExceptionCause cause;
  DoubleRatchetException(this.cause);
}
