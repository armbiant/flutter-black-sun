import 'dart:typed_data';

import 'package:cryptography/cryptography.dart';
import '../security/double_ratchet_exception.dart';
import 'package:tuple/tuple.dart';
import 'double_ratchet_message_header.dart';
import 'double_ratchet_session_state.dart';

/// Implements the Double Ratchet algorithm as proposed by Signal.
/// cf. https://signal.org/docs/specifications/doubleratchet/
class DoubleRatchet {
  /// Initializes the given state for the local part of a session.
  Future initSessionAsLocal(
    DoubleRatchetSessionState state,
    SecretKey sharedSecret,
    SimplePublicKey remoteDhPublicKey,
  ) async {
    state.localDH = await _generateDh();
    state.remoteDH = remoteDhPublicKey;
    final key = await _kdfRk(
      sharedSecret,
      await _dh(state.localDH!, state.remoteDH!),
    );
    state.rootKey = key.item1;
    state.sendingChainKey = key.item2;
    state.receivingChainKey = sharedSecret;
    state.sendingMessageNumber = 0;
    state.receivingMessageNumber = 0;
    state.previousSendingMessageNumber = 0;
    state.clearAllSkippedMessageKeys();
  }

  // Initializes the given state for the remote part of a session.
  Future initSessionAsRemote(
    DoubleRatchetSessionState state,
    SecretKey sharedSecret,
    SimpleKeyPair dhKeyPair,
  ) async {
    state.localDH = dhKeyPair;
    state.remoteDH = null;
    state.rootKey = sharedSecret;
    state.sendingChainKey = sharedSecret;
    state.receivingChainKey = null;
    state.sendingMessageNumber = 0;
    state.receivingMessageNumber = 0;
    state.previousSendingMessageNumber = 0;
    state.clearAllSkippedMessageKeys();
  }

  // Performs a single ratchet encryption on the given session state. Returns
  // the header and the ciphertext of this operation.
  Future<Tuple2<DoubleRatchetMessageHeader, SecretBox>> encrypt(
    DoubleRatchetSessionState state,
    Uint8List plaintext,
    Uint8List authData,
  ) async {
    final keys = await _kdfCk(state.sendingChainKey!);
    state.sendingChainKey = keys.item1;
    final messageKey = keys.item2;
    final header = await _header(
      state.localDH!,
      state.previousSendingMessageNumber,
      state.sendingMessageNumber,
    );
    state.sendingMessageNumber++;
    final ciphertext = await _encrypt(
      messageKey,
      plaintext,
      _concat(authData, header),
    );
    return Tuple2(header, ciphertext);
  }

  // Performs a single ratched decryption on the given session state. Returns
  // the plaintext.
  Future<Uint8List> decrypt(
    DoubleRatchetSessionState state,
    DoubleRatchetMessageHeader header,
    SecretBox ciphertext,
    Uint8List authData,
  ) async {
    final plaintext = await _trySkippedMessageKeys(
      state,
      header,
      ciphertext,
      authData,
    );

    if (plaintext != null) {
      return Uint8List.fromList(plaintext);
    }

    if (header.publicDhKey != state.remoteDH) {
      await _skipMessageKeys(state, header.previousChainLength);
      await _dhRatchetStep(state, header);
    }
    await _skipMessageKeys(state, header.messageNumber);
    final keys = await _kdfCk(state.receivingChainKey!);
    state.receivingChainKey = keys.item1;
    final messageKey = keys.item2;
    state.receivingMessageNumber++;
    return Uint8List.fromList(
      await _decrypt(
        messageKey,
        ciphertext,
        _concat(authData, header),
      ),
    );
  }

  final _DoubleRatchetCryptoSuite _cryptoSuite = _DoubleRatchetCryptoSuite();
}

class _DoubleRatchetCryptoSuite {
  final X25519 keyExchange = X25519();
  final AesGcm cipher = AesGcm.with256bits();
  final Hkdf kdf = Hkdf(hmac: Hmac.sha256(), outputLength: 64);
  final Hmac mac = Hmac.sha256();
}

extension _DoubleRatchetCrypto on DoubleRatchet {
  // Generate a fresh DH key pair.
  Future<SimpleKeyPair> _generateDh() async {
    return _cryptoSuite.keyExchange.newKeyPair();
  }

  // Perform DH on given key material.
  Future<SecretKey> _dh(
    SimpleKeyPair dhKeyPair,
    SimplePublicKey publicDhKey,
  ) async {
    return _cryptoSuite.keyExchange.sharedSecretKey(
      keyPair: dhKeyPair,
      remotePublicKey: publicDhKey,
    );
  }

  // Performs KDF on given root key. Returns a keypair consisting of two
  // 32-byte keys (root key, chain key)

  Future<Tuple2<SecretKey, SecretKey>> _kdfRk(
    SecretKey rootKey,
    SecretKey dhOut,
  ) async {
    final key = await _cryptoSuite.kdf.deriveKey(
      secretKey: dhOut,
      nonce: await rootKey.extractBytes(),
      info: "hkdf_rk".codeUnits,
    );
    final keyBytes = await key.extractBytes();
    return Tuple2(
      SecretKey(keyBytes.sublist(0, 32)),
      SecretKey(keyBytes.sublist(32)),
    );
  }

  // Performs KDF on given chain key. Returns a keypair consisting of two
  // 32-byte keys (chain key, message key).
  Future<Tuple2<SecretKey, SecretKey>> _kdfCk(SecretKey chainKey) async {
    final nextChainKey = await _cryptoSuite.mac.calculateMac(
      [0x02],
      secretKey: chainKey,
    );
    final messageKey = await _cryptoSuite.mac.calculateMac(
      [0x01],
      secretKey: chainKey,
    );
    return Tuple2(
      SecretKey(nextChainKey.bytes),
      SecretKey(messageKey.bytes),
    );
  }

  // Encrypts the plaintext using the given keying material.

  Future<SecretBox> _encrypt(
    SecretKey messageKey,
    Uint8List plaintext,
    List<int> info,
  ) async {
    // Generate a fresh nonce at random
    final nonce = _cryptoSuite.cipher.newNonce();
    return _cryptoSuite.cipher.encrypt(
      plaintext,
      secretKey: messageKey,
      aad: info,
      nonce: nonce,
    );
  }

  // Decrypts the ciphertext using the given keying material.
  Future<List<int>> _decrypt(
    SecretKey messageKey,
    SecretBox ciphertext,
    List<int> info,
  ) async {
    return _cryptoSuite.cipher.decrypt(
      ciphertext,
      secretKey: messageKey,
      aad: info,
    );
  }

  // Builds a new message header from the given data.
  Future<DoubleRatchetMessageHeader> _header(
    SimpleKeyPair dhKeyPair,
    int previousChainLength,
    int messageNumber,
  ) async {
    return DoubleRatchetMessageHeader(
      publicDhKey: await dhKeyPair.extractPublicKey(),
      previousChainLength: previousChainLength,
      messageNumber: messageNumber,
    );
  }

  // Concatenates the specified authentication data and message header as
  // parseable byte sequence.
  Uint8List _concat(
    List<int> ad,
    DoubleRatchetMessageHeader header,
  ) {
    final byteBuilder = BytesBuilder(copy: false);
    byteBuilder.addByte(ad.length);
    byteBuilder.add(ad);
    byteBuilder.addByte(header.publicDhKey.bytes.length);
    byteBuilder.add(header.publicDhKey.bytes);
    byteBuilder.addByte(
      Uint8List(8).buffer.asUint64List()[0] = header.previousChainLength,
    );
    byteBuilder.addByte(
      Uint8List(8).buffer.asUint64List()[0] = header.messageNumber,
    );
    return byteBuilder.toBytes();
  }

  Future<List<int>?> _trySkippedMessageKeys(
    DoubleRatchetSessionState state,
    DoubleRatchetMessageHeader header,
    SecretBox ciphertext,
    List<int> authData,
  ) async {
    final SecretKey? messageKey = state.getSkippedMessageKey(
      header.publicDhKey,
      header.messageNumber,
    );

    if (messageKey != null) {
      state.clearSkippedMessageKey(header.publicDhKey, header.messageNumber);
      return _decrypt(messageKey, ciphertext, _concat(authData, header));
    } else {
      return null;
    }
  }

  Future _skipMessageKeys(DoubleRatchetSessionState state, int until) async {
    if (state.receivingMessageNumber + state.maxSkippableMessages < until) {
      throw DoubleRatchetException(
        DoubleRatchetExceptionCause.maxSkippableMessagesExceeded,
      );
    }

    if (state.receivingChainKey != null) {
      while (state.receivingMessageNumber < until) {
        final keys = await _kdfCk(state.receivingChainKey!);
        state.receivingChainKey = keys.item1;
        final messageKey = keys.item2;
        state.addSkippedMessageKey(
          state.remoteDH!,
          state.receivingMessageNumber,
          messageKey,
        );
        state.receivingMessageNumber++;
      }
    }
  }

  Future _dhRatchetStep(
    DoubleRatchetSessionState state,
    DoubleRatchetMessageHeader header,
  ) async {
    state.previousSendingMessageNumber = state.sendingMessageNumber;
    state.sendingMessageNumber = 0;
    state.receivingMessageNumber = 0;
    state.remoteDH = header.publicDhKey;
    Tuple2<SecretKey, SecretKey> keys = await _kdfRk(
      state.rootKey!,
      await _dh(state.localDH!, state.remoteDH!),
    );
    state.rootKey = keys.item1;
    state.receivingChainKey = keys.item2;
    state.localDH = await _generateDh();
    keys = await _kdfRk(
      state.rootKey!,
      await _dh(state.localDH!, state.remoteDH!),
    );
    state.rootKey = keys.item1;
    state.sendingChainKey = keys.item2;
  }
}

extension _DoubleRatchetSessionStateSkippedMessageKeys
    on DoubleRatchetSessionState {
  // Adds the given message key to the skipped message keys of this session,
  // associating it with the given ratchet public key and message number.
  addSkippedMessageKey(
    SimplePublicKey ratchetPublicKey,
    int messageNumber,
    SecretKey messageKey,
  ) async {
    final index = _buildIndex(ratchetPublicKey, messageNumber);
    skippedMessageKeys[index] = messageKey;
  }

  // Retrieves the skipped message key using the given ratchet public key and
  // message number (or return null, if no such entry exists).
  SecretKey? getSkippedMessageKey(
    SimplePublicKey ratchetPublicKey,
    int messageNumber,
  ) {
    final index = _buildIndex(ratchetPublicKey, messageNumber);
    return skippedMessageKeys[index];
  }

  // Clears the skipped message using the given ratchet public key and message
  // number.
  void clearSkippedMessageKey(
    SimplePublicKey ratchetPublicKey,
    int messageNumber,
  ) {
    final index = _buildIndex(ratchetPublicKey, messageNumber);
    skippedMessageKeys.remove(index);
  }

  // Clears ALL skipped message keys.
  void clearAllSkippedMessageKeys() {
    skippedMessageKeys.clear();
  }

  Uint8List _buildIndex(
    SimplePublicKey ratchetPublicKey,
    int messageNumber,
  ) {
    BytesBuilder bytesBuilder = BytesBuilder(copy: false);
    bytesBuilder.clear();
    bytesBuilder.add(ratchetPublicKey.bytes);
    final messageNumberAsBytes = Uint8List(8)
      ..buffer.asUint64List()[0] = messageNumber;
    bytesBuilder.add(messageNumberAsBytes);
    return bytesBuilder.toBytes();
  }
}
