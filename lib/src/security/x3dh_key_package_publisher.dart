import 'dart:collection';
import 'dart:typed_data';

import 'package:cryptography/cryptography.dart';
import 'package:uuid/uuid.dart';

/// Defines the identity of an X3DH entity (e.g. an user agent operated on
/// a specific device of the user).
class X3DHIdentity {
  String id;

  X3DHIdentity({
    required this.id,
  });

  static X3DHIdentity create() {
    return X3DHIdentity(id: const Uuid().v4());
  }
}

/// Defines the X3DH public key structure
class X3DHPublicKey {
  final String id;
  final SimplePublicKey key;

  X3DHPublicKey({
    required this.id,
    required this.key,
  });
}

/// Defines the container for X3DH key packages communicated between
/// client and server.
class X3DHKeyPackage {
  /// The identity this key package is owned by
  final X3DHIdentity identity;

  /// The public identity key in this key package.
  final X3DHPublicKey identityKey;

  /// The public signed prekey in this key package.
  final X3DHPublicKey prekey;
  final Uint8List prekeySignature;
  final X3DHPublicKey signingKey;

  /// The list of one-time prekeys in this key package.
  final List<X3DHPublicKey> oneTimePrekeys;

  X3DHKeyPackage({
    required this.identity,
    required this.identityKey,
    required this.prekey,
    required this.prekeySignature,
    required this.signingKey,
    required this.oneTimePrekeys,
  });
}

/// Declares an interface for X3DH key package managers.
abstract class X3DHKeyPackagePublisher {
  /// Publishes a new key package for the local identity to a X3DH server.
  /// Keys available in this package replace all keys of same type in a
  /// published key package on the server.
  Future publish(X3DHKeyPackage keyPackage);

  /// Invalidates the key package for the local identity, effectively removing
  /// the key package from the server. Does nothing, if the local identity has
  /// no key package published on the server.
  Future invalidate();

  // Fetches the a key package for a given identity from the X3DH server, if
  // such a package is existing there.
  Future<X3DHKeyPackage?> fetch(X3DHIdentity identity);
}

// Mock implementation for X3DH key publisher relying on a mocked server.
class X3DHInMemoryKeyPackagePublisher implements X3DHKeyPackagePublisher {
  // Maps X3DH identity to the corresponding key package as maintained by
  // mocked server.
  static final HashMap<String, X3DHKeyPackage> _keyPackageMap =
      HashMap<String, X3DHKeyPackage>();

  final X3DHIdentity identity;

  X3DHInMemoryKeyPackagePublisher({
    required this.identity,
  });

  @override
  Future publish(X3DHKeyPackage keyPackage) async {
    assert(keyPackage.identity.id == identity.id);
    _keyPackageMap[identity.id] = keyPackage;
  }

  @override
  Future invalidate() async {
    _keyPackageMap.remove(identity.id);
  }

  @override
  Future<X3DHKeyPackage?> fetch(X3DHIdentity identity) async {
    // Server responds with only one OTK, deleting the returned OTK from its
    // database.
    final kp = _keyPackageMap[identity.id];

    if (kp != null) {
      final otk =
          kp.oneTimePrekeys.isNotEmpty ? kp.oneTimePrekeys.removeAt(0) : null;
      final result = X3DHKeyPackage(
        identity: kp.identity,
        identityKey: kp.identityKey,
        prekey: kp.prekey,
        prekeySignature: kp.prekeySignature,
        signingKey: kp.signingKey,
        oneTimePrekeys: otk != null ? [otk] : [],
      );
      return result;
    } else {
      return null;
    }
  }
}
