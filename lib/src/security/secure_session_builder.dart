import 'package:cryptography/cryptography.dart';
import 'package:flutter/foundation.dart';
import 'double_ratchet.dart';
import 'double_ratchet_message_header.dart';
import 'double_ratchet_session_state.dart';
import 'message_service.dart';
import 'x3dh.dart';
import 'x3dh_key_package_publisher.dart';
import 'x3dh_key_provider.dart';
import 'x3dh_key_storage.dart';
import '../util/data_input_stream.dart';
import '../util/data_output_stream.dart';

/// Implements a secure session.
class SecureSession {
  /// Identifier for the local identity of this session.
  final X3DHIdentity localIdentity;

  /// Identifier for the remote identity connected via this session.
  final String remoteIdentity;

  /// The session state
  final DoubleRatchetSessionState state;

  /// The message service to be used with this session
  final MessagingService sessionMessageService;

  /// The double ratchet to use with this session
  final DoubleRatchet doubleRatchet;

  /// The auth data for this session.
  final Uint8List authData;

  SecureSession({
    required this.doubleRatchet,
    required this.authData,
    required this.localIdentity,
    required this.remoteIdentity,
    required this.state,
    required this.sessionMessageService,
  });

  /// Writes data message to this session. Data is transported to the connected
  /// remote session in a secure manner.
  Future send(Uint8List payload) async {
    // Convert plaintext payload to (authenticated) ciphertext
    final result = await doubleRatchet.encrypt(state, payload, authData);

    // Write ratchet message header followed by ciphertext
    final outputStream = DataOutputStream();
    result.item1.toOutputStream(outputStream);
    result.item2.toOutputStream(outputStream);

    await sessionMessageService.send(
      outputStream.byteBuffer,
      toRecipientId: remoteIdentity,
    );
  }

  /// Reads the next data message from this session. Returns the decrypted
  /// message payload on success.
  Future<Uint8List?> receive() async {
    final message = await sessionMessageService.receiveNext();

    if (message == null) {
      return null;
    }

    // Read double ratchet header followed by ciphertext
    final inputStream = DataInputStream(buffer: message.payload);
    DoubleRatchetMessageHeader header =
        DoubleRatchetMessageHeader.fromInputStream(inputStream);
    SecretBox secretBox = _SecretBoxSerialization.fromInputStream(inputStream);

    // Now decrypt the data.
    final plaintext = await doubleRatchet.decrypt(
      state,
      header,
      secretBox,
      authData,
    );

    return plaintext;
  }
}

extension _SecretBoxSerialization on SecretBox {
  void toOutputStream(DataOutputStream outputStream) {
    outputStream.writeBytes(Uint8List.fromList(mac.bytes));
    outputStream.writeBytes(Uint8List.fromList(nonce));
    outputStream.writeBytes(Uint8List.fromList(cipherText));
  }

  static SecretBox fromInputStream(DataInputStream inputStream) {
    final macBytes = inputStream.readBytes();
    final nonceBytes = inputStream.readBytes();
    final ciphertextBytes = inputStream.readBytes();
    return SecretBox(
      ciphertextBytes,
      nonce: nonceBytes,
      mac: Mac(macBytes),
    );
  }
}

/// Builds sessions for secure bi-directional communication between to parties.
class SecureSessionBuilder {
  /// The application identifier for the sessions built by this builder.
  final String applicationIdentifier;

  /// The local identity
  final X3DHIdentity localIdentity;

  /// The session message service for this builder
  final MessagingService sessionMessageService;

  final X3DHKeyStorage keyStorage;
  final X3DHKeyProvider keyProvider;
  final X3DHKeyPackagePublisher keyPackagePublisher;

  final X3DH _x3dh;
  final DoubleRatchet _doubleRatchet = DoubleRatchet();

  SecureSessionBuilder({
    required this.applicationIdentifier,
    required this.localIdentity,
    required this.keyStorage,
    required this.keyProvider,
    required this.keyPackagePublisher,
    required this.sessionMessageService,
  }) : _x3dh = X3DH(
          applicationId: applicationIdentifier,
          localIdentity: localIdentity,
          keyPackagePublisher: keyPackagePublisher,
          keyProvider: keyProvider,
          storage: keyStorage,
        );

  /// Prepares this session builder instance. Always call this method and wait
  /// for its completion before calling other methods in this instance.
  Future setup() async {
    return _x3dh.setup();
  }

  // Checks, if the specified message contains a HELLO message that can be
  // passed into acceptSession().
  bool isHelloMessage(Message sessionMessage) {
    final inputStream = DataInputStream(buffer: sessionMessage.payload);
    // Correct protocol version?
    assert(inputStream.readByte() == 0x01);
    return inputStream.readInt32() == SessionMessageType.hello.index;
  }

  /// Builds a new session that is connected to the given remote identity.
  Future<SecureSession> build(X3DHIdentity remoteIdentity) async {
    // Now initiate X3DH key agreement with remote identity.
    final x3dhOutput = await _x3dh.initiateProtocol(remoteIdentity);

    // Now, send HELLO message (=initial message) to remote session to complete
    // session setup. In order to send a ciphertext, we need to set up the
    // session state first.
    final state = DoubleRatchetSessionState();
    await _doubleRatchet.initSessionAsLocal(
      state,
      x3dhOutput.privateParameters.sharedKey,
      x3dhOutput.publicParameters.remotePrekey.key,
    );

    // Now construct the plaintext message of the HELLO message.
    final helloMessage = "HELLO ${localIdentity.id} ${remoteIdentity.id}";
    final helloMessageBytes = Uint8List.fromList(helloMessage.codeUnits);
    final ad = Uint8List.fromList(x3dhOutput.privateParameters.associatedData);

    // We can now encrypt the plaintext message
    final helloMessageEncrypted = await _doubleRatchet.encrypt(
      state,
      Uint8List.fromList(helloMessageBytes),
      ad,
    );

    // Finally, we wrap the ciphertext message in an HELLO message envelope...
    final hello = _HelloMessage(
      ciphertextHeader: helloMessageEncrypted.item1,
      ciphertext: helloMessageEncrypted.item2,
      ephemeralKey: x3dhOutput.publicParameters.ephemeralKey,
      oneTimePrekeyId: x3dhOutput.publicParameters.usedOneTimePrekeyId,
      prekeyId: x3dhOutput.publicParameters.remotePrekey.id,
      senderIdentityKey: x3dhOutput.publicParameters.initiatingIdentityKey,
    );

    final helloPayload = hello.toNetworkData();

    // ...and then send it to the remote party.
    await sessionMessageService.send(
      helloPayload,
      toRecipientId: remoteIdentity.id,
    );

    // At this point, we are done with setting up the session.
    final secureSession = SecureSession(
      localIdentity: localIdentity,
      authData: ad,
      sessionMessageService: sessionMessageService,
      doubleRatchet: _doubleRatchet,
      remoteIdentity: remoteIdentity.id,
      state: state,
    );

    return secureSession;
  }

  Future<SecureSession> acceptSession(Message fromHelloMessage) async {
    // We first need to acquire the Hello message from which to extract the
    // data required for setting up the secure session.
    final hello = _HelloMessage.fromNetworkData(fromHelloMessage.payload);

    // Fetch signed prekey, referenced in hello message.
    final prekey = await keyStorage.fetchSignedPrekey(hello.prekeyId);
    final identityKey = await keyStorage.fetchIdentityKey();
    assert(prekey != null);
    assert(identityKey != null);

    // Now perform remote part of X3DH protocol.
    final pubParams = X3DHPublicParameters(
      initiatingIdentityKey: hello.senderIdentityKey,
      remoteIdentityKey: await identityKey!.publicKey(),
      ephemeralKey: hello.ephemeralKey,
      remotePrekey: await prekey!.publicKey(),
      usedOneTimePrekeyId: hello.oneTimePrekeyId,
    );

    final x3dhOutput = await _x3dh.completeProtocol(
      X3DHIdentity(id: fromHelloMessage.receiverIdentity),
      pubParams,
    );

    // Now setup the session state from the received hello message.
    final state = DoubleRatchetSessionState();
    await _doubleRatchet.initSessionAsRemote(
      state,
      x3dhOutput.sharedKey,
      prekey.keyPair,
    );

    // Now attempt to decrypt the ciphertext contained in hello message.
    final plaintext = await _doubleRatchet.decrypt(
      state,
      hello.ciphertextHeader,
      hello.ciphertext,
      Uint8List.fromList(
        x3dhOutput.associatedData,
      ),
    );

    // Make sure, the retrieved plaintext is correct.
    final expectedPlaintext =
        "HELLO ${fromHelloMessage.senderIdentity} ${localIdentity.id}";

    assert(
      listEquals(
        plaintext,
        Uint8List.fromList(
          expectedPlaintext.codeUnits,
        ),
      ),
    );

    // At this point, everything is ok and we can return the established
    // session.
    return SecureSession(
      localIdentity: localIdentity,
      sessionMessageService: sessionMessageService,
      doubleRatchet: _doubleRatchet,
      remoteIdentity: fromHelloMessage.senderIdentity,
      state: state,
      authData: Uint8List.fromList(x3dhOutput.associatedData),
    );
  }
}

class _HelloMessage {
  final DoubleRatchetMessageHeader ciphertextHeader;
  final SecretBox ciphertext;
  final X3DHPublicKey senderIdentityKey;
  final X3DHPublicKey ephemeralKey;
  final String prekeyId;
  final String? oneTimePrekeyId;

  _HelloMessage({
    required this.ciphertextHeader,
    required this.ciphertext,
    required this.ephemeralKey,
    required this.oneTimePrekeyId,
    required this.prekeyId,
    required this.senderIdentityKey,
  });

  // Constructs the network representation of the hello message.
  Uint8List toNetworkData() {
    final DataOutputStream outputStream = DataOutputStream();
    // Protocol version (0x01) and message type
    outputStream.writeByte(0x01);
    outputStream.writeInt32(SessionMessageType.hello.index);
    // Sender identity key
    outputStream.writeString(senderIdentityKey.id);
    outputStream.writeBytes(Uint8List.fromList(senderIdentityKey.key.bytes));
    // Ephemeral key
    outputStream.writeString(ephemeralKey.id);
    outputStream.writeBytes(Uint8List.fromList(ephemeralKey.key.bytes));
    // Prekey ID used by sender
    outputStream.writeString(prekeyId);
    // One-time prekey used by sender
    outputStream.writeString(oneTimePrekeyId ?? "");
    // Write ciphertext header
    ciphertextHeader.toOutputStream(outputStream);
    // Write ciphertext
    ciphertext.toOutputStream(outputStream);
    // Done.
    return outputStream.byteBuffer;
  }

  // Re-constructs the hello message from the given network representation.
  static _HelloMessage fromNetworkData(Uint8List networkBytes) {
    final inputStream = DataInputStream(buffer: networkBytes);
    // Protocol version (0x01) and message type
    assert(inputStream.readByte() == 0x01);
    assert(inputStream.readInt32() == SessionMessageType.hello.index);
    // Sender identity key
    final senderIdentityKeyId = inputStream.readString();
    final senderIdentityKeyBytes = inputStream.readBytes();
    final senderIdentityKey = X3DHPublicKey(
      id: senderIdentityKeyId,
      key: SimplePublicKey(senderIdentityKeyBytes, type: KeyPairType.x25519),
    );
    // Ephemeral key
    final ephemeralKeyId = inputStream.readString();
    final ephemeralKeyBytes = inputStream.readBytes();
    final ephemeralKey = X3DHPublicKey(
      id: ephemeralKeyId,
      key: SimplePublicKey(ephemeralKeyBytes, type: KeyPairType.x25519),
    );
    // Prekey ID
    final prekeyId = inputStream.readString();
    // Onetime prekey ID
    final oneTimePrekeyId = inputStream.readString();
    // Ciphertext header
    final ciphertextHeader =
        DoubleRatchetMessageHeader.fromInputStream(inputStream);
    // Ciphertext
    final ciphertext = _SecretBoxSerialization.fromInputStream(inputStream);

    return _HelloMessage(
      ciphertextHeader: ciphertextHeader,
      ciphertext: ciphertext,
      ephemeralKey: ephemeralKey,
      oneTimePrekeyId: oneTimePrekeyId.isNotEmpty ? oneTimePrekeyId : null,
      prekeyId: prekeyId,
      senderIdentityKey: senderIdentityKey,
    );
  }
}
