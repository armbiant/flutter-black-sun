import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_double_ratchet/src/security/x3dh.dart';
import 'package:flutter_double_ratchet/src/security/x3dh_key_package_publisher.dart';
import 'package:flutter_double_ratchet/src/security/x3dh_key_provider.dart';
import 'package:flutter_double_ratchet/src/security/x3dh_key_storage.dart';

void main() {
  group('X3DH', () {
    test('should execute correctly', () async {
      // In this scenario, we have two parties Alice and Bob that want to
      // establish a shared key through an (unsecure) server.

      // X3DH requires a server to be accessible by Alice and Bob. Access to
      // this server is provided by X3DHKeyPackagePublisher service, whose
      // mock implementation X3DHInMemoryKeyPackagePublisher is what we use in
      // this test.

      // This test is performed from Alice' perspective, making Bob the
      // remote party.

      // Create an identity for Alice and Bob.
      final aliceId = X3DHIdentity.create();
      final bobId = X3DHIdentity.create();

      // Alice and Bob get their own publishers to interact with. Mock
      // implementation takes care of simulating a central server.
      final publisherAlice = X3DHInMemoryKeyPackagePublisher(identity: aliceId);
      final publisherBob = X3DHInMemoryKeyPackagePublisher(identity: bobId);

      // Now create unique key storage instances for Alice and Bob.
      final storageAlice = X3DHInMemoryKeyStorage();
      final storageBob = X3DHInMemoryKeyStorage();

      // And finally we conclude infrastructure setup by creating an instance
      // if the key provider service, which can be shared by Alice and Bob.
      final provider = X3DHDefaultKeyProvider();

      // Now create the X3DH protocol endpoint for Alice.
      final x3dhAlice = X3DH(
        applicationId: "some.test.bundle",
        localIdentity: aliceId,
        storage: storageAlice,
        keyProvider: provider,
        keyPackagePublisher: publisherAlice,
      );

      // Let Alice' X3DH endpoint perform its setup routine. This will create
      // all required keys for Alice, and then generate a test key package
      // and publish it to the mocked server.
      await x3dhAlice.setup();

      // Then create the X3DH protocol endpoint for Bob.
      final x3dhBob = X3DH(
        applicationId: "some.test.bundle",
        localIdentity: bobId,
        storage: storageBob,
        keyProvider: provider,
        keyPackagePublisher: publisherBob,
      );

      // Let Bob's X3DH endpoint perform its setup routine. This will create
      // all required keys for Bob, and then generate a test key package
      // and publish it to the mocked server.
      await x3dhBob.setup();

      // This concludes the setup. We are now ready to have Alice execute the
      // X3DH protocol with Bob, which eventually will provide with a shared key
      // and some other data she will require in post-X3DH protocols, such as
      // Double Ratchet.
      final x3dhOutput = await x3dhAlice.initiateProtocol(bobId);

      // In order to test the correctness, we simulate an initial (empty)
      // message sent from Alice to Bob. In the end, both Alice and and Bob
      // should have obtained the same shared key and associated data.

      // Bob bob completes the X3DH protocol on his side. From his perspective,
      // Alice is the initiating identity.
      final bobParams = await x3dhBob.completeProtocol(
        aliceId,
        x3dhOutput.publicParameters,
      );

      // Now we make the final assertions.
      expect(bobParams.associatedData,
          x3dhOutput.privateParameters.associatedData);
      expect(bobParams.sharedKey, x3dhOutput.privateParameters.sharedKey);
    });
  });
}
