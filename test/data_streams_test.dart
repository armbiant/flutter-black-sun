import 'dart:typed_data';

import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_double_ratchet/src/util/data_input_stream.dart';
import 'package:flutter_double_ratchet/src/util/data_output_stream.dart';

void main() {
  group("Data Streams", () {
    test("Should produce valid data for all supported types", () {
      // Write one element per supported by type
      DataOutputStream output = DataOutputStream();

      output.writeBool(true);
      output.writeByte(42);
      output.writeBytes(Uint8List.fromList("Hello World".codeUnits));
      output.writeDouble(44.45);
      output.writeFloat(48.5);
      output.writeInt32(4242);
      output.writeInt(4242424242424242);
      output.writeString("Hello World");

      // Now read back produced data stream.
      DataInputStream input = DataInputStream(buffer: output.byteBuffer);

      expect(input.readBool(), true);
      expect(input.readByte(), 42);
      expect(input.readBytes(), Uint8List.fromList("Hello World".codeUnits));
      expect(input.readDouble(), 44.45);
      expect(input.readFloat(), 48.5);
      expect(input.readInt32(), 4242);
      expect(input.readInt(), 4242424242424242);
      expect(input.readString(), "Hello World");
    });
  });
}
